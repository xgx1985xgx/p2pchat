﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Net
{
    public class ListenStateException : Exception
    {
        public ListenStateException() : base(@"当前的侦听状态不正确！") { }
    }
}
