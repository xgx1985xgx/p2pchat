﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Data.Packets
{
    [Serializable]
    public class GetUserIPEndPointPacket
    {
        public Guid RemoteUserId { get; set; }
        public string IPAddress { get; set; }
        public int Port { get; set; }
    }
}
