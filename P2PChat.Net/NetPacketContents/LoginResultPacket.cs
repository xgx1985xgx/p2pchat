﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace P2PChat.Data.Packets
{
    [Serializable]
    public class LoginResultPacket
    {
        public int ResultCode { get; set; }
        public string Result { get; set; }

        // 关于登录的返回结果的内容
        public string personalInfoString { get; set; }
        public string friendListString { get; set; }
        public XElement personalInfo
        {
            get
            {
                return XElement.Parse(personalInfoString);
            }
            set
            {
                personalInfoString = value.ToString();
            }
        }
        public XElement friendList
        {
            get
            {
                return XElement.Parse(friendListString);
            }
            set
            {
                friendListString = value.ToString();
            }
        }
    }
}
