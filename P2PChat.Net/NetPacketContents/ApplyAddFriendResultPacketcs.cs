﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace P2PChat.Data.Packets
{
    [Serializable]
    public class ApplyAddFriendResultPacketcs
    {
        public string ResultString { get; set; }
        public string NewFriendListString { get; set; }
        public XElement NewFriendList
        {
            get
            {
                if (NewFriendListString == null)
                {
                    return null;
                }
                return XElement.Parse(NewFriendListString);
            }
            set
            {
                if (value == null) NewFriendList = null;
                NewFriendListString = value.ToString();
            }
        }
    }
}
