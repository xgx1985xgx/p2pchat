﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Data.Packets
{
    [Serializable]
    public class ApplyAddFriendPacket
    {
        public string FriendEmail { get; set; }
        public Guid UserId { get; set; }
    }
}
