﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Data.Packets
{
    [Serializable]
    public class RegistResultPacket
    {
        public string Result { get; set; }
        public int ResultCode { get; set; }
    }
}
