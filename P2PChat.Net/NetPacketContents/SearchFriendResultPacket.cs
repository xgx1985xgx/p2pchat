﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Data.Packets
{
    [Serializable]
    public class SearchFriendResultPacket
    {
        public string ResultXmlString { get; set; }
        public System.Xml.Linq.XElement ResultXml
        {
            get
            {
                return System.Xml.Linq.XElement.Parse(ResultXmlString);
            }
            set
            {
                if (value == null)
                {
                    ResultXmlString = null;
                }
                else
                {
                    ResultXmlString = value.ToString();
                }
            }
        }
    }
}
