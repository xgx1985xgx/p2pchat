﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Data.Packets
{
    [Serializable]
    public class AudioMessagePacket
    {
        public Guid SenderId { get; set; }
        public int SenderSession { get; set; }
        public int ReceiverSession { get; set; }

        public List<Byte> AudioStream { get; set; }
    }
}
