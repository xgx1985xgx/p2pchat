﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Data.Packets
{
    [Serializable]
    public class RawStringPacket
    {
        public string Content { get; set; }
    }
}
