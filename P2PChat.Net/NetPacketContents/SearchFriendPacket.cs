﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Data.Packets
{
    [Serializable]
    public class SearchFriendPacket
    {
        public string SearchText_Nickname { get; set; }
        public string SearchText_Email { get; set; }
    }
}
