﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Utility
{
    /// <summary>
    /// 负责执行命令行并获取标准输出内容。
    /// </summary>
    public class CmdHost
    {
        System.Diagnostics.Process p = new System.Diagnostics.Process();

        public CmdHost(string cmd, string args)
        {
            p.StartInfo.FileName = cmd;
            p.StartInfo.Arguments = args;

            p.StartInfo.UseShellExecute = false;

            p.StartInfo.RedirectStandardInput = true;//可能接受来自调用程序的输入信息
            p.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
            p.StartInfo.CreateNoWindow = true;//不显示程序窗口 
        }

        public void Run()
        {
            if (p.StartInfo.FileName == null)
            {
                throw new ArgumentNullException("尚未制定运行的命令");
            }
            p.Start();
        }

        public void Init(string cmd, string args)
        {
            p.Kill();

            p.StartInfo.FileName = cmd;
            p.StartInfo.Arguments = args;

            p.StartInfo.UseShellExecute = false;

            p.StartInfo.RedirectStandardInput = true;//可能接受来自调用程序的输入信息
            p.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
            p.StartInfo.CreateNoWindow = true;//不显示程序窗口 
        }

        public void Input(string input)
        {
            p.StandardInput.WriteLine(input);
        }

        public void GetOutput(out string result)
        {
            result = p.StandardOutput.ReadToEnd();
        }
    }
}
