﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;

namespace P2PChat.Server
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Program_Init();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormMain());
        }

        /// <summary>
        /// 初始化一些全局变量
        /// </summary>
        static void Program_Init()
        {
            XElement configXmlFile;
            // 读取数据
            try
            {
                configXmlFile = XElement.Load(@"config.xml").Element("Config");
                if (configXmlFile.Element("Port") != null)
                {
                    ServerConfig.Port = int.Parse(
                        configXmlFile.Element("Port").Value);
                }
                else
                {
                    ServerConfig.Port = 3000;
                }
            }
            catch
            {
                new XDocument(new XDeclaration("1.0", "utf-8", "yes"),
                    new XElement("Config",
                        new XElement("Port", 3000))).Save(@"config.xml");
            }
        }
    }
}
