﻿namespace P2PChat.Server
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.ui_Menu = new System.Windows.Forms.MenuStrip();
            this.服务器ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.启动ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.停止ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.退出ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.锁定ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.注册的用户ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.添加用户ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.编辑用户ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.刷新ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ui_Status = new System.Windows.Forms.StatusStrip();
            this.lb_Connections = new System.Windows.Forms.ToolStripStatusLabel();
            this.lb_Working = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lb_SelectedFriendInfo = new System.Windows.Forms.Label();
            this.ui_List_UserFriend = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lb_RightColumn = new System.Windows.Forms.Label();
            this.lb_LeftColumn = new System.Windows.Forms.Label();
            this.ui_List_RegUser = new System.Windows.Forms.ListView();
            this.ui_LC_Guid = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ui_LC_RegEmail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ui_LC_NickName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lb_SelectedUserInfo = new System.Windows.Forms.Label();
            this.ui_Menu.SuspendLayout();
            this.ui_Status.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ui_Menu
            // 
            this.ui_Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.服务器ToolStripMenuItem,
            this.设置ToolStripMenuItem,
            this.注册的用户ToolStripMenuItem,
            this.刷新ToolStripMenuItem});
            this.ui_Menu.Location = new System.Drawing.Point(0, 0);
            this.ui_Menu.Name = "ui_Menu";
            this.ui_Menu.Size = new System.Drawing.Size(721, 25);
            this.ui_Menu.TabIndex = 0;
            this.ui_Menu.Text = "menuStrip1";
            // 
            // 服务器ToolStripMenuItem
            // 
            this.服务器ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.启动ToolStripMenuItem,
            this.停止ToolStripMenuItem,
            this.toolStripSeparator1,
            this.退出ToolStripMenuItem1});
            this.服务器ToolStripMenuItem.Name = "服务器ToolStripMenuItem";
            this.服务器ToolStripMenuItem.Size = new System.Drawing.Size(56, 21);
            this.服务器ToolStripMenuItem.Text = "服务器";
            // 
            // 启动ToolStripMenuItem
            // 
            this.启动ToolStripMenuItem.Name = "启动ToolStripMenuItem";
            this.启动ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.启动ToolStripMenuItem.Text = "启动";
            this.启动ToolStripMenuItem.Click += new System.EventHandler(this.启动ToolStripMenuItem_Click);
            // 
            // 停止ToolStripMenuItem
            // 
            this.停止ToolStripMenuItem.Name = "停止ToolStripMenuItem";
            this.停止ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.停止ToolStripMenuItem.Text = "停止";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(97, 6);
            // 
            // 退出ToolStripMenuItem1
            // 
            this.退出ToolStripMenuItem1.Name = "退出ToolStripMenuItem1";
            this.退出ToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.退出ToolStripMenuItem1.Text = "退出";
            this.退出ToolStripMenuItem1.Click += new System.EventHandler(this.退出ToolStripMenuItem1_Click);
            // 
            // 设置ToolStripMenuItem
            // 
            this.设置ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.锁定ToolStripMenuItem});
            this.设置ToolStripMenuItem.Name = "设置ToolStripMenuItem";
            this.设置ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.设置ToolStripMenuItem.Text = "设置";
            // 
            // 锁定ToolStripMenuItem
            // 
            this.锁定ToolStripMenuItem.Name = "锁定ToolStripMenuItem";
            this.锁定ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.锁定ToolStripMenuItem.Text = "锁定";
            // 
            // 注册的用户ToolStripMenuItem
            // 
            this.注册的用户ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加用户ToolStripMenuItem,
            this.编辑用户ToolStripMenuItem,
            this.删除ToolStripMenuItem});
            this.注册的用户ToolStripMenuItem.Name = "注册的用户ToolStripMenuItem";
            this.注册的用户ToolStripMenuItem.Size = new System.Drawing.Size(80, 21);
            this.注册的用户ToolStripMenuItem.Text = "注册的用户";
            // 
            // 添加用户ToolStripMenuItem
            // 
            this.添加用户ToolStripMenuItem.Name = "添加用户ToolStripMenuItem";
            this.添加用户ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.添加用户ToolStripMenuItem.Text = "添加";
            // 
            // 编辑用户ToolStripMenuItem
            // 
            this.编辑用户ToolStripMenuItem.Name = "编辑用户ToolStripMenuItem";
            this.编辑用户ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.编辑用户ToolStripMenuItem.Text = "编辑";
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.删除ToolStripMenuItem.Text = "删除";
            // 
            // 刷新ToolStripMenuItem
            // 
            this.刷新ToolStripMenuItem.Name = "刷新ToolStripMenuItem";
            this.刷新ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.刷新ToolStripMenuItem.Text = "刷新";
            this.刷新ToolStripMenuItem.Click += new System.EventHandler(this.刷新ToolStripMenuItem_Click);
            // 
            // ui_Status
            // 
            this.ui_Status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lb_Connections,
            this.lb_Working});
            this.ui_Status.Location = new System.Drawing.Point(0, 477);
            this.ui_Status.Name = "ui_Status";
            this.ui_Status.Size = new System.Drawing.Size(721, 22);
            this.ui_Status.TabIndex = 1;
            this.ui_Status.Text = "statusStrip1";
            // 
            // lb_Connections
            // 
            this.lb_Connections.AutoSize = false;
            this.lb_Connections.Name = "lb_Connections";
            this.lb_Connections.Size = new System.Drawing.Size(200, 17);
            this.lb_Connections.Text = "当前连接数";
            this.lb_Connections.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lb_Working
            // 
            this.lb_Working.Name = "lb_Working";
            this.lb_Working.Size = new System.Drawing.Size(68, 17);
            this.lb_Working.Text = "当前的状态";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 346F));
            this.tableLayoutPanel1.Controls.Add(this.lb_SelectedFriendInfo, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.ui_List_UserFriend, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lb_RightColumn, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lb_LeftColumn, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ui_List_RegUser, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lb_SelectedUserInfo, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.20548F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 94.79452F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 86F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(721, 452);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // lb_SelectedFriendInfo
            // 
            this.lb_SelectedFriendInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_SelectedFriendInfo.Location = new System.Drawing.Point(378, 365);
            this.lb_SelectedFriendInfo.Name = "lb_SelectedFriendInfo";
            this.lb_SelectedFriendInfo.Padding = new System.Windows.Forms.Padding(3);
            this.lb_SelectedFriendInfo.Size = new System.Drawing.Size(340, 87);
            this.lb_SelectedFriendInfo.TabIndex = 5;
            this.lb_SelectedFriendInfo.Text = "label2";
            // 
            // ui_List_UserFriend
            // 
            this.ui_List_UserFriend.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.ui_List_UserFriend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ui_List_UserFriend.Location = new System.Drawing.Point(378, 22);
            this.ui_List_UserFriend.MultiSelect = false;
            this.ui_List_UserFriend.Name = "ui_List_UserFriend";
            this.ui_List_UserFriend.Size = new System.Drawing.Size(340, 340);
            this.ui_List_UserFriend.TabIndex = 3;
            this.ui_List_UserFriend.UseCompatibleStateImageBehavior = false;
            this.ui_List_UserFriend.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "用户id";
            this.columnHeader1.Width = 120;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "注册电子邮件";
            this.columnHeader2.Width = 160;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "昵称";
            this.columnHeader3.Width = 120;
            // 
            // lb_RightColumn
            // 
            this.lb_RightColumn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lb_RightColumn.AutoSize = true;
            this.lb_RightColumn.Location = new System.Drawing.Point(385, 3);
            this.lb_RightColumn.Margin = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.lb_RightColumn.Name = "lb_RightColumn";
            this.lb_RightColumn.Size = new System.Drawing.Size(89, 12);
            this.lb_RightColumn.TabIndex = 1;
            this.lb_RightColumn.Text = "选中用户的好友";
            // 
            // lb_LeftColumn
            // 
            this.lb_LeftColumn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lb_LeftColumn.AutoSize = true;
            this.lb_LeftColumn.Location = new System.Drawing.Point(10, 3);
            this.lb_LeftColumn.Margin = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.lb_LeftColumn.Name = "lb_LeftColumn";
            this.lb_LeftColumn.Size = new System.Drawing.Size(65, 12);
            this.lb_LeftColumn.TabIndex = 0;
            this.lb_LeftColumn.Text = "注册的用户";
            // 
            // ui_List_RegUser
            // 
            this.ui_List_RegUser.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ui_LC_Guid,
            this.ui_LC_RegEmail,
            this.ui_LC_NickName});
            this.ui_List_RegUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ui_List_RegUser.Location = new System.Drawing.Point(3, 22);
            this.ui_List_RegUser.MultiSelect = false;
            this.ui_List_RegUser.Name = "ui_List_RegUser";
            this.ui_List_RegUser.Size = new System.Drawing.Size(369, 340);
            this.ui_List_RegUser.TabIndex = 2;
            this.ui_List_RegUser.UseCompatibleStateImageBehavior = false;
            this.ui_List_RegUser.View = System.Windows.Forms.View.Details;
            this.ui_List_RegUser.SelectedIndexChanged += new System.EventHandler(this.ui_List_RegUser_SelectedIndexChanged);
            // 
            // ui_LC_Guid
            // 
            this.ui_LC_Guid.Text = "用户id";
            this.ui_LC_Guid.Width = 120;
            // 
            // ui_LC_RegEmail
            // 
            this.ui_LC_RegEmail.Text = "注册电子邮件";
            this.ui_LC_RegEmail.Width = 160;
            // 
            // ui_LC_NickName
            // 
            this.ui_LC_NickName.Text = "昵称";
            this.ui_LC_NickName.Width = 120;
            // 
            // lb_SelectedUserInfo
            // 
            this.lb_SelectedUserInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_SelectedUserInfo.Location = new System.Drawing.Point(3, 365);
            this.lb_SelectedUserInfo.Name = "lb_SelectedUserInfo";
            this.lb_SelectedUserInfo.Padding = new System.Windows.Forms.Padding(3);
            this.lb_SelectedUserInfo.Size = new System.Drawing.Size(369, 87);
            this.lb_SelectedUserInfo.TabIndex = 4;
            this.lb_SelectedUserInfo.Text = "label1";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 499);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.ui_Status);
            this.Controls.Add(this.ui_Menu);
            this.MainMenuStrip = this.ui_Menu;
            this.MinimumSize = new System.Drawing.Size(600, 360);
            this.Name = "FormMain";
            this.Text = "P2P聊天程序服务器端";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ui_Menu.ResumeLayout(false);
            this.ui_Menu.PerformLayout();
            this.ui_Status.ResumeLayout(false);
            this.ui_Status.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip ui_Menu;
        private System.Windows.Forms.ToolStripMenuItem 服务器ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 启动ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 停止ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 设置ToolStripMenuItem;
        private System.Windows.Forms.StatusStrip ui_Status;
        private System.Windows.Forms.ToolStripStatusLabel lb_Working;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lb_RightColumn;
        private System.Windows.Forms.Label lb_LeftColumn;
        private System.Windows.Forms.ListView ui_List_RegUser;
        private System.Windows.Forms.ColumnHeader ui_LC_Guid;
        private System.Windows.Forms.ColumnHeader ui_LC_RegEmail;
        private System.Windows.Forms.ColumnHeader ui_LC_NickName;
        private System.Windows.Forms.ListView ui_List_UserFriend;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ToolStripMenuItem 注册的用户ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 添加用户ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 编辑用户ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel lb_Connections;
        private System.Windows.Forms.Label lb_SelectedFriendInfo;
        private System.Windows.Forms.Label lb_SelectedUserInfo;
        private System.Windows.Forms.ToolStripMenuItem 锁定ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 刷新ToolStripMenuItem;
    }
}

