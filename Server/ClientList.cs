﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Server
{
    public static class ClientList
    {
        public static Dictionary<Guid, System.Net.IPEndPoint> Clients = new Dictionary<Guid, System.Net.IPEndPoint>();
        public static Dictionary<Guid, String> OnlineStatus = new Dictionary<Guid, string>();

        static ClientList()
        {
            Clients.Add(new Guid(), new System.Net.IPEndPoint(System.Net.IPAddress.Parse("127.0.0.1"), 3000));
        }
    }
}
