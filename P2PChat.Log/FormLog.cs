﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace P2PChat.Log
{
    public partial class form_Log : Form, ILog
    {
        public form_Log()
        {
            InitializeComponent();
        }

        private void btn_Clear_Click(object sender, EventArgs e)
        {
            txt_Content.Clear();
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        public string FilePath
        {
            get
            {
                return null;
            }
        }

        public void Write()
        {
            return;
        }

        private delegate void AddDelegate(params string[] contents);
        public void Add(params string[] contents)
        {
            if (txt_Content.InvokeRequired)
            {
                this.Invoke(new AddDelegate(Add), contents);
            }
            else
            {
                foreach (string str in contents)
                {
                    txt_Content.Text =
                        string.Format("{0} | {1}", DateTime.Now.ToLongTimeString(), str);
                }
            }
        }

        private void form_Log_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.ApplicationExitCall)
            {
                e.Cancel = true;
                this.Hide();
            }

        }
    }
}
