﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace P2PChat.Net
{
    /// <summary>
    /// 存储侦听器所接受的连接事件的相关信息。
    /// </summary>
    public class ListenerEventArgs : EventArgs
    {
        /// <summary>
        /// 存储连接的终端
        /// </summary>
        private TcpClient connectedClient;

        /// <summary>
        /// 获取连接的终端
        /// </summary>
        public TcpClient Client
        {
            get { return connectedClient; }
            private set
            {
                connectedClient = value;
            }
        }

        /// <summary>
        /// 获取连接的终端的IP地址
        /// </summary>
        public IPAddress IP
        {
            get
            {
                IPEndPoint ipEndPoint = connectedClient.Client.RemoteEndPoint as IPEndPoint;
                return ipEndPoint.Address;
            }
        }

        /// <summary>
        /// 获取连接的终端的IP地址的字符串表达
        /// </summary>
        public string IPString
        {
            get
            {
                return IP.ToString();
            }
        }

        /// <summary>
        /// 获取连接的终端所用的端口
        /// </summary>
        public int Port
        {
            get
            {
                IPEndPoint ipEndPoint = connectedClient.Client.RemoteEndPoint as IPEndPoint;
                return ipEndPoint.Port;
            }
        }

        public ListenerEventArgs(TcpClient client)
        {
            connectedClient = client;
        }
    }
}
