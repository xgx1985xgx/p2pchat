﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.IO;

namespace P2PChat.Net
{
    public class NetPeer
    {
        public TcpClient Client { get; private set; }
        public BinaryReader Reader { get; private set; }
        public BinaryWriter Writer { get; private set; }
        public string UserName { get; set; }

        public NetPeer(TcpClient client)
        {
            this.Client = client;
            NetworkStream networkStream = client.GetStream();
            Reader = new BinaryReader(networkStream);
            Writer = new BinaryWriter(networkStream);
        }

        public void Close()
        {
            Reader.Close();
            Writer.Close();
            Client.Close();
        }
    }
}
