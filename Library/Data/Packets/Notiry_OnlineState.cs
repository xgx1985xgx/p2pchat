﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Data.Packets
{
    [Serializable]
    public class Notiry_OnlineState
    {
        public Guid UserId { get; set; }
        public string OnlineState { get; set; }
    }
}
