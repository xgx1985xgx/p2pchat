﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Data.Packets
{
    [Serializable]
    public class FileTransMessagePacket
    {
        public Guid SenderId { get; set; }
        public int SenderSession { get; set; }
        public int ReceiverSession { get; set; }
        public string Filename { get; set; }
        public int Size { get; set; }
        public int ListeningPort { get; set; }
    }
}
