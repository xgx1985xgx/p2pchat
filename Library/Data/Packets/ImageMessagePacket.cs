﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;

namespace P2PChat.Data.Packets
{
    [Serializable]
    public class ImageMessagePacket
    {
        public Guid SenderId { get; set; }
        public int SenderSession { get; set; }
        public int ReceiverSession { get; set; }
        public Bitmap BitmapObject { get; set; }
    }
}
