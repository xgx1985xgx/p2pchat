﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Data.Packets
{
    [Serializable]
    public class TextMessagePacket
    {
        public Guid SenderId { get; set; }
        public int SenderSession { get; set; }
        public int ReceiverSession { get; set; }
        public string Message { get; set; }

        public int ListeningPort { get; set; }
    }
}
