﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Data.Packets
{
    [Serializable]
    public class ApplyPersonalInfoPacket
    {
        public string NewPersonalInfoString { get; set; }
        public System.Xml.Linq.XElement NewPersonalInfo
        {
            get
            {
                if (NewPersonalInfoString == null)
                {
                    return null;
                }
                return System.Xml.Linq.XElement.Parse(NewPersonalInfoString);
            }
            set
            {
                if (value != null)
                {
                    NewPersonalInfoString = value.ToString();
                }
                else
                {
                    NewPersonalInfoString = null;
                }
            }
        }
    }
}
