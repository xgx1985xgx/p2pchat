﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Data.Packets
{
    [Serializable]
    public class LoginPacket
    {
        public string emailAddress { get; set; }
        public string password { get; set; }
        public int listeningPort { get; set; }
    }
}
