﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace P2PChat.Data
{
    [Serializable]
    public class NetPacket
    {
        public PacketTag Tag { get; set; }
        // public Type ContentType { get; set; }
        public object Content { get; set; }
    }

    public enum PacketTag
    {
        /// <summary>
        /// 表示这个包不是任何类型的数据
        /// </summary>
        None,

        // ***********************************
        // 以下只会从客户端向服务器发送的数据包类型
        // ***********************************

        /// <summary>
        /// 客户端 >> 服务器 : 登录请求
        /// </summary>
        Login,
        /// <summary>
        /// 客户端 >> 服务器 : 注册请求
        /// </summary>
        Regist,
        ApplyPersonalInfo,

        // ***********************************
        // 以下只会从服务器向客户端发送的数据包类型
        // ***********************************

        /// <summary>
        /// 若客户端收到该标记的包,说明服务器处理过程中出现了异常,已经中断处理过程
        /// </summary>
        ServerException,
        /// <summary>
        /// 服务器 >> 客户端 : 登录结果
        /// </summary>
        LoginResult,
        /// <summary>
        /// 服务器 >> 客户端 : 注册结果
        /// </summary>
        RegistResult,



        Notify_OnlineState,
        SearchFriend,
        ApplyAddFriend,
        Message,
        GetValue,
        SendFile,
        SendFileResult,
        
    }
}
