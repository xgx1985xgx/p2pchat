﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Media.Recorder
{
    public class SoundRecorderEventArgs : EventArgs
    {
        private short[] content;
        /// <summary>
        /// 初始化SoundRecorderEventArgs的实例
        /// </summary>
        /// <param name="con"></param>
        public SoundRecorderEventArgs(short[] con)
        {
            content = con;
        }
        /// <summary>
        /// 数据
        /// </summary>
        public short[] Content { get { return content; } }
    }
}
