﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Log
{
    public interface ILog
    {
        /// <summary>
        /// 定义日志文件路径
        /// </summary>
        string FilePath { get; }
        /// <summary>
        /// 将日志应用到文件
        /// </summary>
        void Write();

        /// <summary>
        /// 向记录中添加内容
        /// </summary>
        /// <param name="logContent"></param>
        void Add(params string[] contents);
    }
}
