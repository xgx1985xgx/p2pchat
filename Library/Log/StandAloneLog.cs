﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace P2PChat.Log
{
    /// <summary>
    /// 提供相对独立的日志文件的实现
    /// 这种日志的时间戳将放置在文件名上
    /// </summary>
    public class StandAloneLog : ILog
    {
        /// <summary>
        /// 日志文件的基本路径名，不需要带有扩展名
        /// </summary>
        private string filePath;
        /// <summary>
        /// 获取日志文件的路径
        /// </summary>
        public string FilePath
        {
            get 
            {
                DateTime time = DateTime.Now;
                return string.Format("{0}-{1}{2}{3}-{4}{5}{6}.log",
                    filePath,
                    time.Year.ToString("0000"),
                    time.Month.ToString("00"),
                    time.Day.ToString("00"),
                    time.Hour.ToString("00"),
                    time.Minute.ToString("00"),
                    time.Second.ToString("00"));
            }
            set
            {
                filePath = value;
            }
        }

        /// <summary>
        /// 表示一个日志段落
        /// </summary>
        private struct __LogSection
        {
            public string title;
            public string content;
        }
        /// <summary>
        /// 日志的段落缓存
        /// </summary>
        private List<__LogSection> sections = new List<__LogSection>();

        /// <summary>
        /// 将日志文件写入路径
        /// </summary>
        public void Write()
        {
            // 写入文件
            FileInfo file = new FileInfo(this.FilePath);

            try
            {
                FileStream fs = file.Open(FileMode.Append, FileAccess.Write);
                StreamWriter writer = new StreamWriter(fs);

                foreach (__LogSection sec in sections)
                {
                    writer.WriteLine("{0}{1}{0}\n{2}\n",
                        drawTextLine('-'),
                        sec.title,
                        sec.content);
                }
            }
            catch (NotSupportedException ex)
            {
                throw new NotSupportedException(ex.Message + ":" + this.FilePath);
            }

            // 清除日志条目缓存
            sections.Clear();
        }

        /// <summary>
        /// 为日志添加一个段落
        /// </summary>
        /// <param name="logContent">日志的内容，第一个参数为标题，其余的均为内容</param>
        public void Add(params string[] logContent)
        {
            __LogSection section = new __LogSection();
            section.title = logContent[0];
            StringBuilder sb = new StringBuilder();
            for (int i = 1; i < logContent.Length; i++)
            {
                sb.AppendLine(logContent[i]);
            }
            section.content = sb.ToString();
            sections.Add(section);
        }

        /// <summary>
        /// 使用标准字符绘制分割线
        /// </summary>
        /// <param name="ch">绘制所用字符</param>
        /// <param name="length">线长度</param>
        /// <returns>分割线的字符串</returns>
        private string drawTextLine(char ch, int length = 20)
        {
            StringBuilder sb = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                sb.Append(ch);
            }

            return sb.ToString();
        }
    }
}
