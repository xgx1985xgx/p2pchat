﻿/**
 * 客户端节点说明:
 * 
 */
using System;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 客户端节点
/// </summary>
public class ClientNode
{
    /// <summary>
    /// 目标客户端节点的ID
    /// </summary>
    public Guid NodeId = new Guid("00000000-0000-0000-0000-000000000000");
    /// <summary>
    /// 连接目标客户端的主要连接
    /// </summary>
    public Socket MainSocket = null;
    /// <summary>
    /// 与目标客户端的其他连接的集合，每个连接有一个名称所管理
    /// </summary>
    public Dictionary<string, Socket> SubSockets = new Dictionary<string, Socket>();
    /// <summary>
    /// 目标客户端连接时加密传输的公钥
    /// </summary>
    public byte[] RSAPublicKey = null;
    /// <summary>
    /// 连接目标客户端时加密传输的私钥
    /// </summary>
    public byte[] RSAPrivateKey = null;
}