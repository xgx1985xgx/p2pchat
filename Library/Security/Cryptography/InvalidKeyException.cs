﻿/**
 * InvalidKeyException.cs
 * 
 * 作者：Gates_ice
 * 日期：2012年1月18日
 * 说明：
 * 负责处理不正确的密钥
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Security.Cryptography
{
    /// <summary>
    /// 当使用了不正确的密钥时触发此异常
    /// </summary>
    public class InvalidKeyException : Exception
    {
        /// <summary>
        /// 使用密钥初始化此异常
        /// </summary>
        /// <param name="key">不正确的密钥</param>
        public InvalidKeyException(object key) 
            : base(@"使用了不合法的加密密钥!")
        {
            this.invalidKey = key;
        }



        private object invalidKey;
        /// <summary>
        /// 不正确的密钥
        /// </summary>
        public object InvalidKey
        {
            get { return invalidKey; }
        }
    }
}
