﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Client.Data
{
    public static class UserList
    {
        private static List<ClientNode> users = new List<ClientNode>();
        /// <summary>
        /// 获取所有客户端节点
        /// </summary>
        public static ClientNode[] Users
        {
            get
            {
                return users.ToArray();
            }
        }

        /// <summary>
        /// 使用全球统一资源符获取客户端的节点
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static ClientNode Nodes(Guid guid) {
            return users.Where(x => x.NodeId == guid).First();
        }

        /// <summary>
        /// 向列表中添加节点
        /// </summary>
        /// <param name="node"></param>
        public static void AddNode(ClientNode node)
        {
            if (users.Count(x => x.NodeId == node.NodeId) > 0)
            {
                throw new ArgumentException("新添加的节点已经存在！");
            }
            else
            {
                users.Add(node);
            }
        }
    }
}
