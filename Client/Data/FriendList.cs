﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace P2PChat.Client.Data
{
    public class FriendList
    {
        #region 静态成员

        public static XElement DefaultFriendList = new XElement("FriendList",
            new XElement("Group", new XAttribute("Name", "默认分组")));

        // 好友列表元素
        private static XElement friendList;
        /// <summary>
        /// 获取或设置好友列表，赋null值可以使用默认值初始化该变量
        /// </summary>
        public static XElement List { get { return friendList; }
            set
            {
                if (value == null) { friendList = DefaultFriendList; }
                else
                {
                    friendList = value;
                    if (OnListChanged != null)
                    {
                        OnListChanged(friendList, new EventArgs());
                    }
                }
            }
        }

        #endregion


        #region 静态方法
        public static event EventHandler OnListChanged;
        

        #endregion

        #region XML结构说明 不要对这部分取消注释 否则错误列表.Count ++
        //////////// <FriendList>
        ////////////   <Group * name="分组名">
        ////////////     <Friend * guid="好友的GUID">
        ////////////       <Nickname>昵称</Nickname>
        ////////////       <Email>电子邮件地址</Email>
        ////////////       <State>online</State>
        ////////////     </Friend>
        ////////////   </Group>
        //////////// </FriendList>

        //////////// (XElement)State.Value = enum { 
        ////////////   available => 在线;
        ////////////   buzy => 忙碌;
        ////////////   away => 离开;
        ////////////   offline => 离线;
        //////////// };
        #endregion
    }    
    
    /// <summary>
    /// 用户的在线状态
    /// </summary>
    public enum UserState
    {
        Available,
        Busy,
        Away,
        Offline
    }
}
