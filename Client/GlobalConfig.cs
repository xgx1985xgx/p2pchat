﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Client
{
    public static class GlobalConfig
    {
        public static string ServerHost { get; set; }
        public static int ServerPort { get; set; }
        public static int ListeningPort { get; set; }
    }
}
