﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Interop;
using System.Windows;
using System.Windows.Media.Imaging;

namespace P2PChat.Client.Images
{
    public class ResourseImaging
    {
        public static ImageSource GetPortrait(int index) {
            switch (index)
            {
                case 0:
                    return Imaging.CreateBitmapSourceFromHBitmap(
                    Properties.Resources.Portrait_02.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                case 1:
                    return Imaging.CreateBitmapSourceFromHBitmap(
                    Properties.Resources.Portrait_01.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                case 2:
                    return Imaging.CreateBitmapSourceFromHBitmap(
                    Properties.Resources.Portrait_03.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                default:
                    return null;
            }
        }
    }
}
