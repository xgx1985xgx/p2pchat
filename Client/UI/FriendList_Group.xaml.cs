﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;

namespace P2PChat.Client.UI
{
    /// <summary>
    /// FriendList_Group.xaml 的交互逻辑
    /// </summary>
    public partial class FriendList_Group : UserControl
    {
        public FriendList_Group()
        {
            InitializeComponent();
            IsCollapse = false;
            groupInfo = "未设定组名称";

            this.MouseEnter += new MouseEventHandler(FriendList_Group_MouseEnter);
            this.MouseLeave += new MouseEventHandler(FriendList_Group_MouseLeave);
        }

        void FriendList_Group_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
        }

        void FriendList_Group_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Background = new SolidColorBrush(Color.FromArgb(25, 0, 255, 0));
        }

        private bool isCollapse;
        public bool IsCollapse
        {
            get { return isCollapse; }
            set
            {
                isCollapse = value;
                if (value)
                {
                    imgIsCollapse.Source = Imaging.CreateBitmapSourceFromHBitmap(
                        Properties.Resources.Client_FriendList_Group_Minus.GetHbitmap(),
                        IntPtr.Zero,
                        Int32Rect.Empty,
                        BitmapSizeOptions.FromEmptyOptions());
                }
                else
                {
                    imgIsCollapse.Source = Imaging.CreateBitmapSourceFromHBitmap(
                        Properties.Resources.Client_FriendList_Group_Plus.GetHbitmap(),
                        IntPtr.Zero,
                        Int32Rect.Empty,
                        BitmapSizeOptions.FromEmptyOptions());
                }
            }
        }

        private string groupInfo;
        public string GroupInfo
        {
            get { return groupInfo; }
        }

        public void SetGroupInfo(string groupName, int friendCount = 0, int onlineCount = 0)
        {
            GroupName = groupName;
            groupInfo = string.Format(
                @"{0} - {1}/{2}",
                groupName,
                onlineCount,
                friendCount);
            lbGroupInfo.Content = groupInfo;
        }

        private string groupName = "未命名组";
        public string GroupName
        {
            get { return groupName; }
            set { groupName = value;
            lbGroupInfo.Content = value;
            }
        }
    }
}
