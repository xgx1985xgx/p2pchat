﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace P2PChat.Client.UI
{
    /// <summary>
    /// SearchBox.xaml 的交互逻辑
    /// </summary>
    public partial class SearchBox : UserControl
    {
        public SearchBox()
        {
            InitializeComponent();
        }

        private event EventHandler onTextChanged = null;
        public event EventHandler OnTextChanged
        {
            add
            {
                onTextChanged += value;
            }

            remove
            {
                onTextChanged -= value;
            }
        }

        public string SearchText
        {
            get
            {
                return txt_Input.Text;
            }
        }

        private void btn_Clear_Click(object sender, RoutedEventArgs e)
        {
            txt_Input.Text = string.Empty;
        }

        private void txt_Input_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (onTextChanged != null) { onTextChanged(this, new TextBoxEventArgs(SearchText)); }
        }


    }

    public class TextBoxEventArgs : EventArgs
    {
        public string EventText
        {
            get;
            private set;
        }

        public TextBoxEventArgs(string text)
            : base()
        {
            EventText = text;
        }
    }
}
