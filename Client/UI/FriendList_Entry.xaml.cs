﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace P2PChat.Client.UI
{
    /// <summary>
    /// FriendList_Entry.xaml 的交互逻辑
    /// </summary>
    public partial class FriendList_Entry : UserControl
    {
        public FriendList_Entry()
        {
            InitializeComponent();
            IsSelected = false;
            UserName = "";
            Info = "";
            OverridedInfo = string.Empty;

            this.MouseEnter += new MouseEventHandler(FriendList_Entry_MouseEnter);
            this.MouseLeave += new MouseEventHandler(FriendList_Entry_MouseLeave);
        }

        void FriendList_Entry_MouseLeave(object sender, MouseEventArgs e)
        {
            SetColor();
        }

        void FriendList_Entry_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Background = new SolidColorBrush(Color.FromArgb(25, 0, 255, 0));
        }

        private bool isSelected;
        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                SetColor();
            }
        }

        private void SetColor()
        {
            if (IsSelected)
            {
                this.Background = new SolidColorBrush(Color.FromArgb(51, 0, 255, 0));
            }
            else
            {
                this.Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
            }
        }

        private string panel_name;
        public string UserName
        {
            get { return panel_name; }
            set
            {
                panel_name = value;
                lb_Name.Content = panel_name;
            }
        }

        public ImageSource Portrait
        {
            get
            {
                return img_Portrait.Source;
            }

            set
            {
                img_Portrait.Source = value;
            }
        }

        private string panel_info;
        public string Info
        {
            get { return panel_info; }
            set { panel_info = value; }
        }

        private string overrided_info = string.Empty;
        public string OverridedInfo
        {
            get { return overrided_info; }
            set
            {
                overrided_info = value;
                if (value == string.Empty)
                {
                    lb_InfoText.Text = panel_info;
                } else {
                    lb_InfoText.Text = overrided_info;
                }
            }
        }

        public Guid UserId { get; set; }

        public void ClearOverridedInfo()
        {
            OverridedInfo = string.Empty;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
