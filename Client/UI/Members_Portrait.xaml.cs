﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace P2PChat.Client.UI
{
    /// <summary>
    /// Members_Portrait.xaml 的交互逻辑
    /// </summary>
    public partial class Members_Portrait : UserControl
    {
        public Members_Portrait()
        {
            InitializeComponent();
        }

        public string UserName
        {
            get { return lb_Name.Content as string; }
            set { lb_Name.Content = value; }
        }

        public ImageSource Source
        {
            get { return img_Portrait.Source; }
            set { img_Portrait.Source = value; }
        }

        public void SetPortrait(BitmapSource bitmap)
        {
            img_Portrait.Source = bitmap;
        }

    }
}
