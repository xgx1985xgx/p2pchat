﻿/**
 * ICryptography.cs
 * 
 * 作者：Gates_ice
 * 日期：2012年1月18日
 * 说明：
 * 加密行为的接口方法
 * [2012-02-02] 接口更改，添加
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Security.Cryptography
{
    /// <summary>
    /// 加密行为的接口方法
    /// </summary>
    public interface IEncryptography
    {
        /// <summary>
        /// 获得某个字符串的密文
        /// </summary>
        /// <param name="plainStr">明文</param>
        /// <returns>密文</returns>
        string Encrypt(string plainStr);
        /// <summary>
        /// 获得某个字节数组的密文
        /// </summary>
        /// <param name="plainData">明文数据</param>
        /// <returns>密文数据</returns>
        byte[] Encrypt(byte[] plainData);
        /// <summary>
        /// 获得某段加密字符串的原文
        /// </summary>
        /// <param name="encryptedStr">密文</param>
        /// <returns>原文</returns>
        string Decrypt(string encryptedStr);
        /// <summary>
        /// 获得某段加密数据的原文
        /// </summary>
        /// <param name="encryptedData">密文数据</param>
        /// <returns>原文数据</returns>
        byte[] Decrypt(byte[] encryptedData);
        /// <summary>
        /// 对密文进行解密
        /// </summary>
        /// <param name="encryptedStr">密文</param>
        /// <param name="result">明文</param>
        /// <returns>解密是否成功</returns>
        bool TryDecrypt(string encryptedStr, out string result);
        /// <summary>
        /// 对加密数据进行解密
        /// </summary>
        /// <param name="encryptedData">加密数据</param>
        /// <param name="result">解密数据</param>
        /// <returns>解密是否成功</returns>
        bool TryDecrypt(byte[] encryptedData, out byte[] result);
    }
}
