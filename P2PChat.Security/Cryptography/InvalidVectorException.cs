﻿/**
 * InvalidVectorException.cs
 * 
 * 作者：Gates_ice
 * 日期：2012年1月18日
 * 说明：
 * 负责处理不正确的向量
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P2PChat.Security.Cryptography
{
    /// <summary>
    /// 当使用了不正确的向量时触发此异常
    /// </summary>
    public class InvalidVecterException : Exception
    {
        /// <summary>
        /// 使用向量初始化此异常
        /// </summary>
        /// <param name="key">不正确的向量</param>
        public InvalidVecterException(object vecter)
            : base(@"使用了不合法的加密向量!")
        {
            this.invalidVecter = vecter;
        }

        private object invalidVecter;
        /// <summary>
        /// 不正确的向量
        /// </summary>
        private object InvalidVecter
        {
            get { return invalidVecter; }
        }
    }
}
